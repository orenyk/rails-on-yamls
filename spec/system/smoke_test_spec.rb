require 'rails_helper'

RSpec.describe 'Smoke test', type: :system do
  it 'works' do
    visit root_path

    expect(page).to have_content('Rails on YAMLs')
  end
end
