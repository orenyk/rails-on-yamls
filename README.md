# Rails on YAMLs

This application is a proof-of-concept of using collection of YAML files as the
persistent database for a locally-run Ruby on Rails applications. Rails uses a
Postgres DB in development but resets the database every time Rails is
initialized (dropping the DB, loading the schema, and then seeding the DB from
the YAML files on disk). Each time the database is updated in development the
YAML files are updated as well.
